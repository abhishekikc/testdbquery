package hello;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;


public interface CustomerObjectRepository extends JpaRepository<CustomerObject, Long> {
    @Transactional
    public CustomerObject findOne(Long id);
}
